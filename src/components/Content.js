import Part from './Part';

const Content = (props) => {  
    return (
        <>
            {props.partes.map((parte)=><Part datos={parte} />)}
            <p>Cantidad de cursos: {(props.partes.reduce((acum,valor)=>({exercises: acum.exercises+valor.exercises}))).exercises}</p>
        </>
    )
}

export default Content