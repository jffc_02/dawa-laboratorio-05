import Header from './Header';
import Content from './Content';

const Course = (props) => {  
    return (
    <>
        <Header titulo={props.course.name}/>
        <Content partes={props.course.parts}/>
        <hr></hr>
    </>
    )
}

export default Course